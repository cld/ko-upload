ko.bindingHandlers.upload = {
    init: function(el, valueAccessor, allBindings, data, context) {
        var url = ko.unwrap(valueAccessor()),
            done = allBindings.get('uploadDone'),
            fail = allBindings.get('uploadFail'),
            progress = allBindings.get('uploadProgress'),
            start = allBindings.get('uploadStart'),
            end = allBindings.get('uploadEnd');

        $(el).change(function() {
            ko.bindingHandlers.upload.start(el, url, done, fail, progress, start, end);
        });
    },
    start: function(el, url, done, fail, progress, start, end) {
        var data = new FormData(),
            options = {
                url: url,
                method: 'POST',
                type: 'multipart/form-data',
                dataType: 'json',
                contentType: false,
                processData: false,
                data: data
            }, xhr;

        data.append(el.name, el.files[0]);

        if (typeof progress == 'function') {
            options.xhr = function() {
                var xhr = $.ajaxSettings.xhr();

                if (xhr.upload) {
                    xhr.upload.addEventListener('progress', function(event) {
                        var position = event.loaded;
                        var total = event.total;
                        var percent = Math.ceil(position / total * 100);

                        progress(percent, position, total);
                    }, false);
                }
                return xhr;
            }
        }

        if (typeof start == 'function') {
            start();
        }
        xhr = $.ajax(options);

        if (typeof done == 'function') {
            xhr.done(done);
        }

        if (typeof fail == 'function') {
            xhr.fail(fail);
        }

        if (typeof end == 'function') {
            xhr.always(end);
        }
    }
};
