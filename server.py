# -*- coding: utf-8 -*-
from flask import Flask, request, render_template, jsonify


app = Flask(__name__)


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/upload', methods=['POST'])
def upload():
    f = request.files['file']
    f.read()
    if f:
        message = u'Received file: {filename}'.format(filename=f.filename)
    else:
        message = u'Error: file not sent'
    return jsonify(message=message)


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
